#!/usr/bin/python3
# coding=utf8

import sys
import time

from urlparse3 import urlparse3
from pprint import pprint
import requests
import urllib
import base64
import hashlib
import os
import re
import json
import argparse
from bs4 import BeautifulSoup

flick_client_id="371nd6agb33kglca7vkhvolkdcoddqm"
flick_state="uTgEMVEaEJNXT7oIjlyFZ0NDZF1TnczwEYG3Vwr3iv8"
flick_login_url="https://api.flick.energy/identity/users/sign_in"
flick_test_url="https://api.flick.energy/identity/userinfo"
flick_auth_url="https://api.flick.energy/identity/oauth/authorize"
flick_token_url="https://api.flick.energy/identity/oauth/token"
flick_forecast_url="https://api.flick.energy/rating/forecast_prices"
flick_mobile_url="https://api.flick.energy/customer/mobile_provider/price"
flick_userinfo_url="https://api.flick.energy/identity/userinfo"
flick_redirect_uri="flickapp://nz.co.flickelectric.iphoneapp"
flick_authority="api.flick.energy"

def flick_get_bearer():
    flick_bearer_token=""
    try:
        f=open ("flick_bearer.txt","r")
        flick_bearer_token=f.read ()
        f.close()
    except IOError:
        print ("Could not read bearer token from flick_bearer.txt, requesting a new token")

    PROXIES={
#            "http":"http://proxy:8080",
#            "https":"http://proxy:8080",
            }

    if (len (flick_bearer_token)>0):
        # We got a token stored, let's test it to see if it's still valid
        URL=flick_test_url
        HEADERS={
                "authority":flick_authority,
                "authorization": "Bearer "+str (flick_bearer_token),
                }
        r=requests.get (url=URL,proxies=PROXIES,headers=HEADERS,allow_redirects=False)
        if (r.status_code!=200):
            # Stored flick_bearer_token appears to be invalid, requesting a new one
            flick_bearer_token=""

    if (len (flick_bearer_token)==0):
        # Generate a code verifier and challange
        flick_code_verifier=base64.urlsafe_b64encode (os.urandom(40)).decode('utf-8')
        flick_code_verifier=re.sub ('[^a-zA-Z0-9]+','',flick_code_verifier)
        flick_code_challenge=hashlib.sha256 (flick_code_verifier.encode ('utf-8')).digest ()
        flick_code_challenge=base64.urlsafe_b64encode (flick_code_challenge).decode ('utf-8')
        flick_code_challenge=flick_code_challenge.replace ('=', '')

        HEADERS={}
        URL=flick_auth_url
        PARAMS={
                "response_type":"code",
                "code_challenge_method":"S256",
                "scope":"openid profile",
                "code_challenge":flick_code_challenge,
                "redirect_uri":flick_redirect_uri,
                "client_id":flick_client_id,
                "state":flick_state,
                }
        r=requests.get (url=URL,proxies=PROXIES,headers=HEADERS,params=PARAMS)
        if (r.status_code!=200):
            print ("Request to "+URL+" failed with status code "+str(r.status_code)+" - This is critical")
            return ""
        
        flick_cookie=r.headers ['Set-Cookie']
        flick_cookie='; '.join(c.split(';') [0] for c in flick_cookie.split (', '))
        soup=BeautifulSoup (r.text,"lxml")
        metas = soup.find_all('meta')

        for meta in metas:
            if 'name' in meta.attrs and meta.attrs ['name']=="csrf-token":
                csrf_token=meta.attrs ['content']

        HEADERS={
                "Cookie":flick_cookie,
                }
        URL=flick_login_url
        PARAMS={
                "response_type":"code",
                "code_challenge_method":"S256",
                "scope":"openid profile",
                "code_challenge":flick_code_challenge,
                "redirect_uri":flick_redirect_uri,
                "client_id":flick_client_id,
                "state":flick_state,
                }
        DATA={
                "utf8":"✓",
                "authenticity_token":"/wr/"+csrf_token,
                "response_type":"code",
                "client_id":flick_client_id,
                "redirect_uri":flick_redirect_uri,
                "scope":"openid profile",
                "user[guest]":"false",
                "user[email]":args.username,
                "user[password]":args.password,
                "user[remember_me]":"0",
                "button":"",
                }

        r=requests.post (url=URL,proxies=PROXIES,headers=HEADERS,params=PARAMS,data=DATA,allow_redirects=False)
        if (r.status_code!=302):
            print ("Request to "+URL+" failed with status code "+str(r.status_code)+" - This is critical")
            return ""
        url_parse=urlparse (r.headers ["Location"])
        flick_code=parse_qs (url_parse.query) ['code'][0]

        URL=flick_token_url
        DATA={
                "code":flick_code,
                "code_verifier":flick_code_verifier,
                "redirect_uri":flick_redirect_uri,
                "client_id":flick_client_id,
                "grant_type":"authorization_code",
                }

        r=requests.post (url=URL,proxies=PROXIES,params=PARAMS,data=DATA,allow_redirects=False)
        if (r.status_code!=200):
            print ("Request to "+URL+" failed with status code "+str(r.status_code)+" - This is critical")
            return ""

        flick_auth_list=json.loads (r.text)
        flick_bearer_token=flick_auth_list ["id_token"]

        try:
            f=open ("flick_bearer.txt","w")
            f.write (flick_bearer_token)
            f.close()
        except IOError:
            print ("Could not read bearer token from flick_bearer.txt, requesting a new token")

    return flick_bearer_token

def flick_get_forecast (bearer,supply_node,periods):
    HEADERS={
            "authority":flick_authority,
            "authorization": "Bearer "+str (bearer),
            }
    PARAMS={
            "supply_node":supply_node,
            "number_of_periods_ahead":periods,
            }
    PROXIES={
#            "http":"http://proxy:8080",
#            "https":"http://proxy:8080",
            }
    URL=flick_forecast_url
    r=requests.get (url=URL,proxies=PROXIES,headers=HEADERS,params=PARAMS,allow_redirects=False)
    if (r.status_code!=200):
        print ("Request to "+URL+" failed with status code "+str(r.status_code)+" - This is critical")
        print (r.text)
        return ""
    else:
        return (r.text)

def flick_get_pricing (bearer):
    HEADERS={
            "authority":flick_authority,
            "authorization": "Bearer "+str (bearer),
            }
    PARAMS={
            }
    PROXIES={
#            "http":"http://proxy:8080",
#            "https":"http://proxy:8080",
            }
    URL=flick_mobile_url
    r=requests.get (url=URL,proxies=PROXIES,headers=HEADERS,params=PARAMS,allow_redirects=False)
    if (r.status_code!=200):
        print ("Request to "+URL+" failed with status code "+str(r.status_code)+" - This is critical")
        return ""
    else:
        return (r.text)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Various options for Flick.')
    parser.add_argument("-u","--username",help="Username to authenticate to Flick",required=True)
    parser.add_argument("-p","--password",help="Password to authenticate to Flick",required=True)
    parser.add_argument("-f","--forecast",action="store_true",help="Use the forecast pricing instead of the needle pricing")
    args = parser.parse_args()

    flick_bearer_token=flick_get_bearer ()

    HEADERS={
            "authority":flick_authority,
            "authorization": "Bearer "+str (flick_bearer_token),
            }

    PROXIES={
#            "http":"http://proxy:8080",
#            "https":"http://proxy:8080",
            }
    URL=flick_userinfo_url
    r=requests.get (url=URL,proxies=PROXIES,headers=HEADERS,allow_redirects=False)
    if (r.status_code!=200):
        print ("Request to "+URL+" failed with status code "+str(r.status_code)+" - This is critical")
        exit;

    flick_userinfo_list=json.loads (r.text)
    flick_supplynodes=flick_userinfo_list ["authorized_data_contexts"]["supply_nodes"]
    flick_supply_id=flick_supplynodes [0]["identifier"]

    if args.forecast:
        prices=json.loads (flick_get_forecast (bearer=flick_bearer_token,supply_node=1,periods=6))
        price=prices
    else:
        prices=json.loads (flick_get_pricing (bearer=flick_bearer_token))
        price=prices ["needle"]["price"]

    print (price)
