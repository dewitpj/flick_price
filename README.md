# Flick Price (flick_price.py)

Retrieve the current pricing from Flick Electric

This script will store the bearer token to avoid authenticating
multiple times - just like the Mobile App

# Requirements:

pip3 install -r requirements.txt

# Usage:

./flick_price.py -u username -p password

Enclose username in "" if it's an email address

Improvements welcome !
